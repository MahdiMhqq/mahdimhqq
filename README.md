<h1 align="center">👋 Hi, I’m MAHDI MOHAQEQ</h1>
<h5 align="center">A Front-End Developer from IR-TEHRAN</h5>

<div id="header" align="center">
  <img src="https://media3.giphy.com/media/qgQUggAC3Pfv687qPC/giphy.gif?cid=ecf05e474s7e7z106vunbn16j5h935s4xkej523dcdp8nr1l&rid=giphy.gif" width="400"/>
</div>


- 👀 I’m 28 years Front End ddeveloper.
- ✅ The technologies I've learned so far in last two years:
  - HTML, CSS, SASS
  - BOOTSTRAP, TailwindCSS
  - JavaScript, TypeScript
  - React.js, Next.js, Vue.js
  - Redux, Redux-toolkit, Zustand
  - Material UI, AntDesign, Mantine
  - Docker
  - Cypress, Storybook, Jest
  - Web3
                                                                                                                                          
</div>

- 🌱 I'm planing to extend my knowledge in this area with learning ThreeJS, Jenkins and dive deeper in JS language
- 📫 How to reach me:

<div id="badges" align="center">
  <a href="https://www.linkedin.com/in/mahdimhqq/">
    <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/>
  </a>
  <a href="mailto:mahdi.mohaqeq.mm@gmail.com">
    <img src="https://img.shields.io/badge/Gmail-red?style=for-the-badge&logo=gmail&logoColor=white" alt="Gmail Badge"/>
  </a>
  <a href="https://github.com/MahdiMhqq">
    <img src="https://img.shields.io/badge/Github-orange?style=for-the-badge&logo=github&logoColor=white" alt="Github Badge"/>
  </a>
</div>

